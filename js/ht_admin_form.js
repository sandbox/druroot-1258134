function toggleVis(elem) {
  elem = jQuery(elem);
  elem_inner = elem.next('.rule-block-inner').children('.rule-block-rules');
  if(elem_inner.is(':visible')) {
    elem_inner.hide();
    elem.text('Show Actions');
  } else {
    elem_inner.show();
    elem.text('Hide Actions');
  }
}

function replace_rules() {
  base = jQuery('textarea.calc-output').val();
  jQuery('.rewrite-item').each(function () {
    index = jQuery(this).attr('data-rule-index');
    lookup_index = '.item-' + index
    orig = jQuery(lookup_index).val();
    replace = jQuery(this).val();
    base = base.replace(orig, replace);
    jQuery(lookup_index).val(replace)
  });
  jQuery('textarea.calc-output').val(base);
}

jQuery(document).ready(function () {
  jQuery('.toggle-vis').each(function () {
    toggleVis(this);
  }).click(function () {
    toggleVis(this);
  });

  jQuery('.last a').click(function () {
    replace_rules();
  });
});