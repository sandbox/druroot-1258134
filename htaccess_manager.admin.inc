<?php
/**
 * @file
 * htaccess_manager.forms.inc
 * form callback for htaccess_manager_module
 */

function htaccess_manager_config_form() {
  drupal_add_js(drupal_get_path('module', 'htaccess_manager') . '/js/ht_admin_form.js');
  drupal_add_css(drupal_get_path('module', 'htaccess_manager') . '/css/ht_admin_form.css');
  $form['htaccess_manager'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['rewrite_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rewrite Rules'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'htaccess_manager',
  );
  
  $ruleset = htaccess_manager_get_rewrite_rules();
  foreach ($ruleset as $k => $rules) {
    $stripe = $k % 2 ? 'even' : 'odd';
    $form['rewrite_rules'][$k]['prefix'] = array(
      '#markup' => "<div class=\"rule-block $stripe\"><span class=\"toggle-vis\"></span><div class=\"rule-block-inner\">",
    );
    $rule_init = $cond_init = FALSE;
    foreach ($rules as $j => $rule) {
      if(stristr($rule, 'rewritecond') && $cond_init === FALSE) {
        $form['rewrite_rules'][$k]['cond_prefix'] = array(
          '#markup' => '<div class="rule-block-conditions"><span class="cond-title">Conditions</span>',
        );
        $cond_init = TRUE;
      } else if (stristr($rule, 'rewriterule') && $rule_init == FALSE) {
        if($cond_init === FALSE) {
          //we have no conditions, create a placeholder
          $form['rewrite_rules'][$k]['cond_prefix'] = array(
            '#markup' => '<div class="rule-block-conditions"><span class="cond-title">These Reactions have no conditions associated with them.</span></div>',
          );
        }
        $form['rewrite_rules'][$k]['rule_prefix'] = array(
          '#markup' => '<div class="rule-block-rules"><span class="cond-title">Reactions</span>',
        );
        $rule_init = TRUE;
      }
      $form['rewrite_rules'][$k][] = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#default_value' => $rule,
        '#attributes' => array('class' => array('rewrite-item'), 'data-rule-index' => "$k-$j"),
      );
      $form['rewrite_rules'][$k][] = array(
        '#type' => 'hidden',
        '#title' => t(''),
        '#default_value' => $rule,
        '#attributes' => array('class' => array("rewrite-item-orig item-$k-$j"), 'data-rule-index' => "$k-$j"),
      );
      if($rule_init) {
        $form['rewrite_rules'][$k]['rule_suffix'] = array(
          '#markup' => '</div>',
        );
      }
      if($cond_init) {
        $form['rewrite_rules'][$k]['cond_suffix'] = array(
          '#markup' => '</div>',
        );
      }
    }
    
    $form['rewrite_rules'][$k]['suffix'] = array(
      '#markup' => '</div></div>',
    );
  }
  
  $form['calc_output'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calculated Output'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'htaccess_manager',
  );
  
  $form['calc_output']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Calculated Output'),
    '#default_value' => htaccess_manager_get_htaccess_file(),
    '#description' => 'Copy and paste this code into your htaccess file.',
    '#attributes' => array('class' => array("calc-output")),
  );
  
  $form['calc_output'][] = array(
    '#type' => 'submit',
    '#value' => t('Generate htaccess file'),
    '#submit' => array('htaccess_manager_generate_file_submit'),
  );


  /*
   *
  $form['directory_index'] = array(
    '#type' => 'fieldset',
    '#title' => t('Directory Index'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'htaccess_manager',
  );
   * 
  //@todo: read this from the htaccess file
  $index_cand = array('k' => 'v');
  foreach ($index_cand as $k => $v) {
    $key = 'htaccess_manager_' . $k;
    $form['directory_index'][$key] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#default_value' => $v,
    );
  }

  $form['server_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'htaccess_manager',
  );
  //@todo: read this from the htaccess file
  $options = array('k' => 'v');
  foreach ($options as $k => $v) {
    $key = 'htaccess_manager_' . $k;
    $form['server_options'][$key] = array(
      '#type' => 'textfield',
      '#title' => t(''),
      '#default_value' => $v,
    );
  }
   *
   */

  return $form;
}

function htaccess_manager_generate_file_submit($form, &$form_state) {
  $ht_mark = $form_state['values']['body'];

  $path = 'public://htam';
  $filename = '.htaccess';
  file_prepare_directory($path, FILE_CREATE_DIRECTORY);

  $file = $path . DIRECTORY_SEPARATOR . $filename;

  file_unmanaged_save_data($ht_mark, $file, FILE_EXISTS_REPLACE);

  drupal_set_message(t("The htaccess file at @path has been updated.", array('@path' => $file)));
}

function htaccess_manager_settings_form() {
  $form['htaccess_manager_safe_mode'] = array(
    '#type' => 'checkbox',
    '#title' => 'Safe Mode',
    '#description' => 'Disable automatic writing of htaccess (reccomended)',
    '#options' => array(
      0 => 'write_mode',
      1 => 'safe_mode',
    ),
    '#default_value' => variable_get('htaccess_manager_safe_mode', 1),
  );

  return system_settings_form($form);
}